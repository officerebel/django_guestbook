from django.urls import path 

from . import views

urlpatterns = [
    path('rebels/', views.index,  name='index')
]