## Django Guestbook

# run with virtual env
mkvirtualenv django_guestbook
git clone https://gitlab.com/officerebel/django_guestbook.git
cd django_guestbook
pip install django

run with python manage.py runserver
